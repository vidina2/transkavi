package com.vidina.transkavi;

import java.util.Hashtable;
import java.util.List;

import android.content.Context;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class ExpandableListAdapterViewSchedule extends
		BaseExpandableListAdapter {
	private Context context;
	private List<List<ScheduleTuple>> listSchedulePerDestination;
	private Hashtable<Integer, String> idToName;
	private int timeNow;

	public ExpandableListAdapterViewSchedule(Context context,
			List<List<ScheduleTuple>> listSchedulePerDestination,
			Hashtable<Integer, String> idToName) {
		this.context = context;
		this.listSchedulePerDestination = listSchedulePerDestination;
		this.idToName = idToName;
		
		Time now = new Time();
		now.setToNow();
		
		timeNow = now.hour * 60 + now.minute ;
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return listSchedulePerDestination.get(groupPosition)
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final ScheduleTuple item = (ScheduleTuple) getChild(groupPosition,
				childPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(
					R.layout.row_layout_view_schedule_expanded, null);
		}

		TextView textView = (TextView) convertView
				.findViewById(R.id.row_layout_view_schedule_expanded_label), text2 = (TextView) convertView.findViewById(R.id.textView123);

		int time = item.getTime();
		int hour = (time / 60);
		int minute = (time % 60);
		
		int duration = time - timeNow;
		if ( duration < 0 ) duration += 60*24;
		
		textView.setText(formTime(time) );
		text2.setText(formText(duration));
		return convertView;
	}

	private String formTime(int t){
		int hour = t/60;
		int minute = t%60;
		
		String sHour = (hour < 10) ? "0" + hour : "" + hour;
		String sMinute = (minute < 10) ? "0" + minute : "" + minute;
	
		return sHour + " : " + sMinute;
	}
	
	private String formText(int duration){
		if (duration < 60){
			return "" + duration + " menit";
		}else{
			if (duration % 60 == 0){
				return "" + (duration/60) + " jam";
			}else{
				return "" + (duration/60) + " jam " + (duration%60) + " menit";
			}
		}
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		return listSchedulePerDestination.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return listSchedulePerDestination.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return listSchedulePerDestination.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		List<ScheduleTuple> item = (List<ScheduleTuple>) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(
					R.layout.row_layout_view_schedule, null);
		}

		TextView textView = (TextView) convertView
				.findViewById(R.id.row_layout_view_schedule_label);

		int idDestination = item.get(0).getDestination();
		textView.setText(idToName.get(idDestination));

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
}