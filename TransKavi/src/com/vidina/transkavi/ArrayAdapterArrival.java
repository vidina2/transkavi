package com.vidina.transkavi;

import java.util.List;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.graphics.Paint;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableRow;
import android.widget.TextView;


public class ArrayAdapterArrival extends ArrayAdapter<ArrivalTuple>{
	private final Context context;
	private final List<ArrivalTuple> arrivalList;
	private int timeNow;

	public ArrayAdapterArrival(Context context, List<ArrivalTuple> arrivalList) {
		super(context, R.layout.row_layout_view_arrival, arrivalList);
		this.context = context;
		this.arrivalList = arrivalList;
		
		Time now = new Time();
		now.setToNow();
		
		timeNow = now.hour * 60 + now.minute ;
	}
	
	@Override
	public View getView (int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.row_layout_view_arrival, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.row_layout_view_arrival_label), textTujuan = (TextView) rowView.findViewById(R.id.textView1);
		
		ArrivalTuple t = arrivalList.get(position);
		int arrivalTime = timeNow + t.getEstimatedTime();

		textView.setText(formText(t.getEstimatedTime()));
		textTujuan.setText(t.getDestination() );
		return rowView;
	}
	
	private String formTime(int t){
		int hour = t/60;
		int minute = t%60;
		
		String sHour = (hour < 10) ? "0" + hour : "" + hour;
		String sMinute = (minute < 10) ? "0" + minute : "" + minute;
	
		return sHour + " : " + sMinute;
	}
	
	private String formText(int duration){
		if (duration < 60){
			return duration + " menit";
		}else{
			if (duration % 60 == 0){
				return (duration/60) + " jam";
			}else{
				return (duration/60) + " jam " + (duration%60) + " menit";
			}
		}
	}
	
	public long getItemId(int position) {
		return (long)position;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}
}
