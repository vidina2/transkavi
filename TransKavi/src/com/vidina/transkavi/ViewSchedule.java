package com.vidina.transkavi;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.ExpandableListView;

public class ViewSchedule extends ActionBarActivity {
	private String halteName;
	private int category;
	private AssetManager am;
	private Hashtable<String, List<Integer>> nameToId;
	private Hashtable<Integer, String> idToName;
	private List<List<ScheduleTuple>> scheduleList;

	private ExpandableListView listView;
	private ExpandableListAdapterViewSchedule listAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lihat_jadwal);

		getSupportActionBar().setIcon(R.drawable.logo_xhdpi);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		init();
	}

	private void init() {
		Intent intent = this.getIntent();

		listView = (ExpandableListView) findViewById(R.id.list_lihat_jadwal);
		this.listView.setEmptyView(findViewById(R.id.list_lihat_jadwal_empty));
		halteName = intent.getStringExtra("namaHalte");
		category = intent.getIntExtra("category", -1);
		am = this.getApplicationContext().getAssets();

		setTitle("Lewat " + halteName);
		// Log.d("info333", "hello world!"+halteName);
		// DEBUG
		// halteName = "Sunan Giri";
		// category = 1;

		nameToId = new Hashtable<String, List<Integer>>();
		idToName = new Hashtable<Integer, String>();
		scheduleList = new ArrayList<List<ScheduleTuple>>();

		loadNames();
		loadSchedules();

		placeToView();
	}

	private void loadNames() {
		// load names
		try {
			InputStream is = am.open("nama.txt");
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			int nRead;
			byte[] data = new byte[2048];
			while ((nRead = is.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			buffer.flush();

			String[] content = new String(buffer.toByteArray()).split("\n");
			for (String item : content) {
				String[] t = item.split(";");
				String s = t[1];
				int id = Integer.parseInt(t[0]);

				List<Integer> ids = null;

				if (nameToId.containsKey(s)) {
					ids = nameToId.get(s);
				} else {
					ids = new ArrayList<Integer>();
					nameToId.put(s, ids);
				}

				ids.add(id);
				idToName.put(id, s);
			}

		} catch (Exception e) {
			Log.d("Lihat Jadwal", "gan error buka nama.txt  " + e.toString());
		}
	}

	private void loadSchedules() {
		// load schedules
		try {
			InputStream is = am.open("jadwal.txt");
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			int nRead;
			byte[] data = new byte[2048];
			while ((nRead = is.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			buffer.flush();

			String[] content = new String(buffer.toByteArray()).split("\n");
			for (String item : content) {
				String[] t = item.split(";");

				String[] head = t[0].split(",");
				String[] tail = t[1].split(",");

				int category = Integer.parseInt(head[0]);
				int current = Integer.parseInt(head[1]);
				int destination = Integer.parseInt(head[2]);

				if (!idToName.get(current).equals(halteName)
						|| (this.category != category)) {
					// no need to proceed
					continue;
				}

				List<ScheduleTuple> temp = new ArrayList<ScheduleTuple>();
				for (String child : tail) {
					int time = Integer.parseInt(child);
					temp.add(new ScheduleTuple(-1, time, -1, -1, destination));
				}
				Collections.sort(temp, new SortComparator());
				scheduleList.add(temp);
			}

		} catch (Exception e) {
			Log.d("Lihat Jadwal", "gan error buka nama.txt  " + e.toString());
		}
	}

	private void placeToView() {
		listAdapter = new ExpandableListAdapterViewSchedule(this, scheduleList,
				idToName);
		listView.setAdapter(listAdapter);

		// for (int i = 0; i < listAdapter.getGroupCount(); i++){
		// listView.expandGroup(i);
		// }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lihat_jadwal, menu);
		return true;

	}

	class SortComparator implements Comparator<ScheduleTuple> {

		@Override
		public int compare(ScheduleTuple a, ScheduleTuple b) {
			return a.getTime() - b.getTime();
		}
	}
}