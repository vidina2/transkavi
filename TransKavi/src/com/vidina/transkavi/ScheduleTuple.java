package com.vidina.transkavi;

public class ScheduleTuple implements Comparable<ScheduleTuple> {
	private int category;
	private int time;
	private int index;
	private int current;
	private int destination;

	public ScheduleTuple(int _category, int _time, int _index, int _current, int _destination){
		category = _category;
		time = _time;
		index = _index;
		current = _current;
		destination = _destination;
	}

	public int compareTo(ScheduleTuple o){
		if (category != o.category){
			return category - o.category;
		}else if (current != o.current){
			return current - o.current;
		}else{
			return destination - o.destination;
		}
	}

	public int getCategory(){
		return category;
	}
	
	public int getTime(){
		return time;
	}
	
	public int getIndex(){
		return index;
	}
	
	public int getCurrent(){
		return current;
	}
	
	public int getDestination(){
		return destination;
	}
	
	public String toString(){
		return  "" + category + " " + time + " " + index + " " + current + " " + destination;
	}
}