package com.vidina.transkavi;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends ActionBarActivity {

	private AutoCompleteTextView halte;
	private Button viewScheduleButton;
	private Button nextBusButton;
	private ToggleButton toggleButton;
	private int screenWidth;
	
	private String[] halteList;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        halteList = getResources().getStringArray(R.array.halte_list);
        halte = (AutoCompleteTextView) findViewById(R.id.departureHalteField);
        halte.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, halteList));
        
        toggleButton = (ToggleButton) findViewById(R.id.dayToggleButton);
        viewScheduleButton = (Button) findViewById(R.id.submitButton);
        nextBusButton = (Button) findViewById(R.id.nextBusButton);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        screenWidth= display.getWidth();
        
        toggleButton.setClickable(false);
        toggleButton.setOnTouchListener(new OnTouchListener(){
	        @Override
	        public boolean onTouch(View v, MotionEvent event){
		        int x = (int)event.getX();
		        int y = (int)event.getY();
		        
		        if (x < screenWidth/2){
		        	toggleButton.setChecked(false);
		        }else{
		        	toggleButton.setChecked(true);
		        }
		        return false;
	        }
	   });
      
        viewScheduleButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager)getSystemService(
					      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(((AutoCompleteTextView) findViewById(R.id.departureHalteField)).getWindowToken(), 0);
//				tampil(halte.getText().toString());
				String halteIsian = halte.getText().toString();
				if(!validateHalte(halteIsian)) return;
				
				Intent i = new Intent(MainActivity.this, ViewSchedule.class);
				i.putExtra("namaHalte", halteIsian);
				i.putExtra("category", toggleButton.isChecked() ? 0 : 1);
				final int a = 1;
				startActivityForResult(i, a);
			}
		});
        
        nextBusButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				tampil(halte.getText().toString());
				String halteIsian = halte.getText().toString();
				if(!validateHalte(halteIsian)) return;
				
				Intent i = new Intent(MainActivity.this, NextBus.class);
				i.putExtra("namaHalte", halteIsian);
				final int a = 1;
				startActivityForResult(i, a);
			}
		});
       getSupportActionBar().setIcon(R.drawable.logo_xhdpi);
    }

	private void tampil(String pesan) {
		Toast.makeText(this, "Anda memilih halte " + pesan, Toast.LENGTH_SHORT).show();
	}
	
	private boolean validateHalte(String isian) {
		halte.setError(null);
		
		if(isian.equals("")) {
			halte.setError(getString(R.string.error_halte_empty));
			return false;
		} else {
			boolean contained = false;
			for(String halteI : halteList) if(halteI.equals(isian)) {
				contained = true;
				break;
			}
			
			if(!contained) {
				halte.setError(getString(R.string.error_halte_not_exist));
				return false;
			}
		}
		
		return true;
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
