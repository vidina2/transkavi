package com.vidina.transkavi;

public class ArrivalTuple implements Comparable<ArrivalTuple>{
	private String destination;
	private int estimatedTime;
	
	public ArrivalTuple(String destination, int estimatedTime){
		this.destination = destination;
		this.estimatedTime = estimatedTime;
	}
	
	public String getDestination(){
		return destination;
	}
	
	public int getEstimatedTime(){
		return estimatedTime;
	}

	@Override
	public int compareTo(ArrivalTuple o) {
		if (estimatedTime != o.getEstimatedTime()){
			return estimatedTime - o.getEstimatedTime();
		}else{
			return destination.compareTo(o.getDestination());
		}
	}
}
