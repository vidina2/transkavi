// http://api.hackjak.bappedajakarta.go.id/busway/halte/405?apiKey=wwqU7Dos4D6Wc9Qgy6xBGirU8qV3rQw0

package com.vidina.transkavi;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.ListView;
import android.widget.TextView;

public class NextBus extends ActionBarActivity {
	private ProgressDialog progress;private String halteName;
	private AssetManager am;
	private Hashtable<String, List<Integer>> nameToId;
	private Hashtable<Integer, String> idToName;
	private List<ArrivalTuple> arrivalList;
	private ArrayAdapterArrival arrayAdapter;
	private boolean finishedRequest;

	private TextView textView;
	private ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bus_berikutnya);

		progress = new ProgressDialog(this);
		progress.setTitle("Mengambil data dari server");
		progress.setMessage("Mohon tunggu...");
		progress.show();

		getSupportActionBar().setIcon(R.drawable.logo_xhdpi);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {
				try {
					Log.d("asd", "gan jalan");
					
					getData();
					placeToView();
					progress.dismiss();
				} catch (Exception e) {
				}

			}
		}, 2000);
		Log.d("asd", "gan beres");
	}

	private void init() {
		Intent intent = this.getIntent();
		listView = (ListView) findViewById(R.id.list_bus_berikutnya);
		textView = (TextView) findViewById(R.id.text_halte);

		halteName = intent.getStringExtra("namaHalte");

		am = this.getApplicationContext().getAssets();

		arrivalList = new ArrayList<ArrivalTuple>();
		nameToId = new Hashtable<String, List<Integer>>();
		idToName = new Hashtable<Integer, String>();

		textView.setText("Melewati " + halteName);

		loadNames();
	}

	private void loadNames() {
		// load names
		try {
			InputStream is = am.open("nama.txt");
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			int nRead;
			byte[] data = new byte[2048];
			while ((nRead = is.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			buffer.flush();

			String[] content = new String(buffer.toByteArray()).split("\n");
			for (String item : content) {
				String[] t = item.split(";");
				String s = t[1];
				int id = Integer.parseInt(t[0]);

				List<Integer> ids = null;

				if (nameToId.containsKey(s)) {
					ids = nameToId.get(s);
				} else {
					ids = new ArrayList<Integer>();
					nameToId.put(s, ids);
				}

				ids.add(id);
				idToName.put(id, s);
			}

		} catch (Exception e) {
			Log.d("Lihat Jadwal", "gan error buka nama.txt  " + e.toString());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bus_berikutnya, menu);
		return true;
	}

	public void getData() {
		finishedRequest = false;

		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

			// DEBUG
			List<Integer> halteId = nameToId.get(halteName);
			for (Integer ii : halteId) {
				// WUT?
				int koridorId = ii / 100;
				URL url = new URL(
						"http://api.hackjak.bappedajakarta.go.id/busway/estimasi/koridor/"
								+ koridorId + "/halte/" + ii
								+ "?apiKey=wwqU7Dos4D6Wc9Qgy6xBGirU8qV3rQw0");

				Log.d("asd", "gan " + url.toString());
				HttpURLConnection urlConnection = (HttpURLConnection) url
						.openConnection();

				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(true);

				// connect
				urlConnection.connect();

				// Stream byte
				InputStream inputStream = urlConnection.getInputStream();

				String s = "";
				int v;
				int p = 0;
				while ((v = inputStream.read()) != -1) {
					char c = ((char) v);
					if ((c != '\n') && (c != '\r')) {
						s += c;
					}
				}

				Log.d("asd", "gan muncul " + s);
				// DEBUG
				// s =
				// "{  \"result_halte\": [    {      \"HalteId\": \"401\",      \"HalteName\": \"Pulo Gadung\",      \"Long\": \"106.90821\",      \"Lat\": \"-6.18426\",      \"KoridorNo\": \"4\",      \"Capacity\": 100,      \"HalteType\": 0    },    {      \"HalteId\": \"405\",      \"HalteName\": \"Pemuda Rawamangun\",      \"Long\": \"106.89165\",      \"Lat\": \"-6.19345\",      \"KoridorNo\": \"4\",      \"Capacity\": 100,      \"HalteType\": 1    },    {      \"HalteId\": \"417\",      \"HalteName\": \"Dukuh Atas 2\",      \"Long\": \"106.82335\",      \"Lat\": \"-6.20442\",      \"KoridorNo\": \"4\",      \"Capacity\": 100,      \"HalteType\": 0    }  ],  \"result\": [    {      \"KoridorNo\": \"4\",      \"HalteID\": \"405\",      \"BusNo\": \"JTM039\",      \"ETA\": 32,      \"Longitude\": \"\",      \"Latitude\": \"\",      \"ETATime\": \"2014-04-26 21:50:46\",      \"BusTime\": \"\",      \"HalteName\": \"Pulo Gadung\"    },    {      \"KoridorNo\": \"4\",      \"HalteID\": \"405\",      \"BusNo\": \"PP016\",      \"ETA\": 26,      \"Longitude\": \"\",      \"Latitude\": \"\",      \"ETATime\": \"2014-04-26 21:50:48\",      \"BusTime\": \"\",      \"HalteName\": \"Dukuh Atas 2\"    },    {      \"KoridorNo\": \"4\",      \"HalteID\": \"405\",      \"BusNo\": \"JTM042\",      \"ETA\": 32,      \"Longitude\": \"\",      \"Latitude\": \"\",      \"ETATime\": \"2014-04-26 21:50:47\",      \"BusTime\": \"\",      \"HalteName\": \"Pulo Gadung\"    },    {      \"KoridorNo\": \"4\",      \"HalteID\": \"405\",      \"BusNo\": \"JTM040\",      \"ETA\": 27,      \"Longitude\": \"\",      \"Latitude\": \"\",      \"ETATime\": \"2014-04-26 21:50:46\",      \"BusTime\": \"\",      \"HalteName\": \"Dukuh Atas 2\"    },    {      \"KoridorNo\": \"\",      \"HalteID\": \"\",      \"HalteName\": \"\",      \"BusNo\": \"\",      \"ETA\": 0,      \"Longitude\": \"\",      \"Latitude\": \"\",      \"ETATime\": \"\",      \"BusTime\": \"\"    },    {      \"KoridorNo\": \"4\",      \"HalteID\": \"405\",      \"BusNo\": \"JTM028\",      \"ETA\": 33,      \"Longitude\": \"106.86086273193359\",      \"Latitude\": \"-6.1946001052856445\",      \"ETATime\": \"2014-04-26 21:50:47\",      \"BusTime\": \"2014-04-26 21:50:47\",      \"HalteName\": \"Dukuh Atas 2\"    }  ],  \"error\": \"\",  \"time\": \"5599ms\"}";

				JSONObject json = new JSONObject(s);
				if (json.has("result")) {
					Object result = json.get("result");
					JSONArray arr = new JSONArray(result.toString());

					for (int i = 0; i < arr.length(); i++) {
						JSONObject item = arr.getJSONObject(i);

						if (isComplete(item)) {
							String destination = item.getString("HalteName");
							int estimatedTime = item.getInt("ETA");
							arrivalList.add(new ArrivalTuple(destination,
									estimatedTime));
						}

					}
				}
			}
		} catch (final Exception e) {
			Log.d("asd", "gan " + e.toString());
		}
		progress.dismiss();
	}

	private boolean isComplete(JSONObject arrival) {
		boolean result = false;
		try {
			result = (!arrival.getString("HalteName").equals(""));
		} catch (JSONException e) {
		}

		return result;
	}

	private void placeToView() {
		Collections.sort(arrivalList);
		arrayAdapter = new ArrayAdapterArrival(this, arrivalList);
		listView.setAdapter(arrayAdapter);
	}
}
